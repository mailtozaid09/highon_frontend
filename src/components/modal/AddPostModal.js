import React, { useState, useEffect } from 'react'
import { Text, View, SafeAreaView, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, FlatList, Modal, } from 'react-native';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

const AddPostModal = ({closeModal}) => {

    const navigation = useNavigation()

    return (
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={true}
                onRequestClose={() => {console.log("closed");}}>
                <View style={styles.modalContainer} >
                    <View style={styles.container} >
                        <TouchableOpacity 
                            onPress={closeModal}
                            style={{marginBottom: 10, justifyContent: 'center', alignItems: 'flex-end'}} >
                            <Image source={media.close} style={styles.closeStyle} />
                        </TouchableOpacity>
                        
                        <TouchableOpacity 
                            onPress={() => {closeModal(); navigation.navigate('SelectPostType', {postType: 'image'})}}
                            style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Image source={media.add_post} style={styles.iconStyle} />
                            <Text style={styles.heading} >Create a post</Text>
                        </TouchableOpacity>

                        <View style={{height: 0.8, width: '100%', marginVertical: 10, backgroundColor: colors.gray}} />

                        <TouchableOpacity 
                            onPress={() => {closeModal(); navigation.navigate('SelectPostType', {postType: 'video'})}}
                            style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Image source={media.add_story} style={styles.iconStyle} />
                            <Text style={styles.heading} >Create a story</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    ) 
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff', 
    },
    container: {
        width: '90%',
        paddingHorizontal: 20,
        paddingVertical: 15,
        borderRadius: 14,
        justifyContent: 'center',
        backgroundColor: '#D0D0D0',
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginLeft: 30,
    },
    iconStyle: {
        height: 24,
        width: 24,
    },
    closeStyle: {
        height: 30,
        width: 30,
    }
})

export default AddPostModal