import React from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { useNavigation } from '@react-navigation/native'

import BackIcon from 'react-native-vector-icons/AntDesign'


const AddPostHeader = ({nextButton}) => {

    const navigation  = useNavigation()

    return (
        <View style={styles.container} >
            <TouchableOpacity
                onPress={() => navigation.goBack()}
            >
                <BackIcon name="arrowleft" size={28} color={colors.black} style={styles.iconStyle} />
            </TouchableOpacity>

            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <TouchableOpacity
                    onPress={nextButton}   
                    style={styles.nextButton} 
                >
                    <Text style={styles.heading} >Next</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: colors.gray,
        paddingHorizontal: 20,
        backgroundColor: colors.white,
        justifyContent: 'space-between',
    },
    nextButton: {
        backgroundColor: colors.primary,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 25,
        borderRadius: 25,
    },
    heading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
})

export default AddPostHeader