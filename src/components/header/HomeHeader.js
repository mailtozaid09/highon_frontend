import React from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { useNavigation } from '@react-navigation/native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'

import SearchIcon from 'react-native-vector-icons/Feather'
import PostIcon from 'react-native-vector-icons/Octicons'


const HomeHeader = ({addPostButton}) => {

    const navigation  = useNavigation()

    return (
        <View style={styles.container} >
            <Image source={{uri: 'https://highon.co.in/static/media/logoImg.9635e655c9b2f2d82717.png'}} style={{height: 55, width: 55, resizeMode: 'contain' }} />

            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <TouchableOpacity
                    onPress={addPostButton}    
                >
                    <PostIcon name="diff-added" size={28} color={colors.black} style={styles.iconStyle} />
                </TouchableOpacity>
                <View>
                    <SearchIcon name="search" size={28} color={colors.black} style={styles.iconStyle} />
                </View>

                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: colors.gray,
        paddingHorizontal: 20,
        backgroundColor: colors.white,
        justifyContent: 'space-between',
    },
    iconStyle: {
        marginLeft: 15,
    }
})

export default HomeHeader