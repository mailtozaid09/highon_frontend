import React from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList, ActivityIndicator,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { useNavigation } from '@react-navigation/native'

import BackIcon from 'react-native-vector-icons/AntDesign'

const SavePostHeader = ({addPostButton, isVibeTag, buttonLoader}) => {

    const navigation  = useNavigation()

    return (
        <View style={styles.container} >
            <TouchableOpacity
                onPress={() => navigation.goBack()}
            >
                <BackIcon name="arrowleft" size={28} color={colors.black} style={styles.iconStyle} />
            </TouchableOpacity>

            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <TouchableOpacity
                    onPress={addPostButton}   
                    disabled={buttonLoader ? true : !isVibeTag}
                    style={[styles.nextButton, {backgroundColor: isVibeTag ? colors.primary : colors.dark_gray}]} 
                >
                    {buttonLoader
                    ?
                    <ActivityIndicator size="small" color={colors.white} />
                    :
                    <Text style={styles.heading} >Post</Text>
                    }
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: colors.gray,
        paddingHorizontal: 20,
        backgroundColor: colors.white,
        justifyContent: 'space-between',
    },
    nextButton: {
        height: 36,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 25,
        borderRadius: 25,
    },
    heading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
})

export default SavePostHeader