import React, {useRef, useState, } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'
import { screenHeight, screenWidth } from '../../global/constants'
import { useNavigation } from '@react-navigation/native'

import MenuIcon from 'react-native-vector-icons/Entypo'
import HeartIcon from 'react-native-vector-icons/AntDesign'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import Video from 'react-native-video';

const PostItem = ({index, object, postArray}) => {

    const { post_url, post_type, id, aspect_ratio, } = object

    const navigation = useNavigation()

    const videoPlayer = useRef(null);

    const [favourites, setFavourites] = useState([]);

    const updateFavourites = (object) => {

        if(!favourites.includes(object.id)){       
            setFavourites(oldArray => [...oldArray, object.id]);  
            console.log("adddd");
        }else{
            console.log("remove");
            var myArray = favourites.filter(( obj ) => {
                return obj !== object.id;
            });
            
            setFavourites(myArray)
        }
    }

    return (
        <TouchableOpacity 
            key={id}
            style={[
                styles.postImage,
                {
                    height: 
                        aspect_ratio == '1:1' ? screenWidth/2-25 
                        : aspect_ratio == '4:5' ? screenWidth/1.6 
                        : aspect_ratio == '16:9' ? screenWidth/2-60 
                        : screenWidth/2-25, 
                    width: 
                        aspect_ratio == '1:1' ? screenWidth/2-25 
                        : aspect_ratio == '4:5' ? screenWidth/2-25 
                        : aspect_ratio == '16:9' ? screenWidth/2-25 
                        : screenWidth/2-25, 
                }
            ]} 
            onPress={() => {navigation.navigate(post_type == 'image' ? 'ViewPostDetails': 'ViewStoryDetails', {details: object, postId: id, postArray: postArray})}}
            >
            {post_type == 'image' ?
                <Image source={{uri: object?.post_url}} style={{height: '100%', width: '100%', borderRadius: 15}} />
                :
                <Video
                    ref={videoPlayer}
                    source={{ uri: object?.post_url}}
                    resizeMode='cover'
                    repeat={false}
                    playWhenInactive={true}
                    muted={true}
                    playInBackground={false}
                    style={{
                        borderRadius: 15,
                        height: 
                            aspect_ratio == '1:1' ? screenWidth/2-25 
                            : aspect_ratio == '4:5' ? screenWidth/1.6 
                            : aspect_ratio == '16:9' ? screenWidth/2-60 
                            : screenWidth/2-25, 
                        width: 
                            aspect_ratio == '1:1' ? screenWidth/2-25 
                            : aspect_ratio == '4:5' ? screenWidth/2-25 
                            : aspect_ratio == '16:9' ? screenWidth/2-25 
                            : screenWidth/2-25, 
                    }}
                    volume={50}
                />
            }

            <View style={{ position: 'absolute', right: 15, bottom: 15}} >
                {favourites.includes(object.id) ?
                <TouchableOpacity
                    onPress={() => {updateFavourites(object)}}
                >
                    <HeartIcon name="heart" size={24} color={colors.reddish} style={styles.iconStyle} />
                </TouchableOpacity>
                :
                <TouchableOpacity
                    onPress={() => {updateFavourites(object)}}
                >
                    <HeartIcon name="hearto" size={24} color={colors.white} style={styles.iconStyle} />
                </TouchableOpacity>
                }
            </View>

            <View style={{ position: 'absolute', left: 15, bottom: 10}} >
                <Text style={{color: colors.white, fontFamily: Poppins.Medium, fontSize: 14,}} >{post_type == 'image' ? 'Post' : 'Story'}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
    },
    postImage: {
        marginBottom: 10,
        borderRadius: 15,
    }
})

export default PostItem