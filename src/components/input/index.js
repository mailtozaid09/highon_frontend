import React, { useState, useEffect } from 'react'
import { Text, View, SafeAreaView, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, FlatList, Modal, TextInput, } from 'react-native';
import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';

const Input = ({label, value, placeholder, keyboradType, onChangeText, isMultiline}) => {

    const navigation = useNavigation()

    return (
        <View>
            <Text style={styles.heading} >{label}</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    value={value}
                    placeholder={placeholder}
                    keyboardType={keyboradType}
                    style={styles.inputStyle}
                    multiline={isMultiline}
                    onChangeText={(text) => onChangeText(text)}
                />
            </View>
        </View>
    ) 
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff', 
    },
    container: {
        width: '90%',
        paddingHorizontal: 20,
        paddingVertical: 15,
        borderRadius: 14,
        justifyContent: 'center',
        backgroundColor: '#D0D0D0',
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.primary1,
        marginTop: 10,
    },
    inputContainer: {
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 2,
        borderColor: colors.primary1,
    },
    inputStyle: {
        padding: 15,
        height: 100,
        fontSize: 14,
        color: colors.dark_gray,
        fontFamily: Poppins.Medium,
        textAlignVertical: 'top',
    },
})

export default Input