export const colors = {
    primary: '#00B2E8',
    primary1: '#0198C6',

    bg_color: '#F7F7F7',
    dark_gray: '#707070',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    
    black: '#252a34',
    white: '#fdfdfd',
    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',
}