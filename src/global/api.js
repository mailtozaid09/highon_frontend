const ipAddress = '10.201.101.2'

const baseUrl = `http://${ipAddress}:3000/api/v1/`



export function getAllPost() {
    return(
        fetch(
            baseUrl + 'posts',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

export function updatePostDetails(body, id) {
    return(
        fetch(
            baseUrl + 'posts/'+id,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}


export function addNewPost(body) {
    return(
        fetch(
            baseUrl + 'posts',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        )
        .then(res => res.json())
    );
}
