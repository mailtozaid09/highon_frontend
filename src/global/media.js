export const media = {
    close: require('../assets/close.png'),
    add_post: require('../assets/add_post.png'),
    add_story: require('../assets/add_story.png'),

    gallery: require('../assets/gallery.png'),
    camera: require('../assets/camera.png'),

    home: require('../assets/home.png'),
    home_fill: require('../assets/home_fill.png'),
    admin: require('../assets/admin.png'),
    admin_fill: require('../assets/admin_fill.png'),

    meditation: require('../assets/meditation.png'),
    food: require('../assets/food.png'),
    photography: require('../assets/photography.png'),
    game_controller: require('../assets/game-controller.png'),
    light: require('../assets/light.png'),
    rainy: require('../assets/rainy.png'),
    poppers: require('../assets/poppers.png'),
    sun: require('../assets/sun.png'),
    adventurer: require('../assets/adventurer.png'),
    creative_idea: require('../assets/creative-idea.png'),


}
