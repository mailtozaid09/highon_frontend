import { colors } from "./colors";
import { media } from "./media";

export const peoplesTag = [
    {
        title: 'zaid_ahmed',
    },
    {
        title: 'praveen_vanaparla',
    },
    {
        title: 'debyendu_khanapur',
    },
    {
        title: 'ahmed_zaid',
    },
]

export const locationTags = [
    {
        title: 'Marathaalli',
    },
    {
        title: 'Indiranagar',
    },
    {
        title: 'Majestic',
    },
    {
        title: 'KR Puram',
    },
    {
        title: 'Kundalhalli',
    },
]

export const vibeTags = [
    {
        title: 'Adventure',
        icon: media.adventurer
    },
    {
        title: 'Ralaxing',
        icon: media.meditation
    },
    {
        title: 'Gaming',
        icon: media.game_controller
    },
    {
        title: 'Party',
        icon: media.poppers
    },
    {
        title: 'Rainy',
        icon: media.rainy
    },
    {
        title: 'Photography',
        icon: media.photography
    },

    {
        title: 'Inspiring',
        icon: media.creative_idea
    },
    {
        title: 'Festival',
        icon: media.light
    },
    {
        title: 'Sunny',
        icon: media.sun
    },
    {
        title: 'Food',
        icon: media.food
    },
]
    
export const sampleItems = [
    {
        color: 'red',
        aspectRatio: 40/145,
        aspect_ratio: '1:1',
    },
    {
        color: 'blue',
        aspectRatio: 60/145,
        aspect_ratio: '16:9',
    },
    {
        color: 'green',
        aspectRatio: 90/145,
        aspect_ratio: '4:5',
    },
    {
        color: 'red',
        aspectRatio: 120/145,
        aspect_ratio: '1:1',
    },
    // {
    //     color: 'blue',
    //     aspectRatio: 90/145,
    //     aspect_ratio: '16:9',
    // },
    // {
    //     color: 'green',
    //     aspectRatio: 60/145,
    //     aspect_ratio: '4:5',
    // },
    // {
    //     color: 'green',
    //     aspectRatio: 90/145,
    //     aspect_ratio: '4:5',
    // },
    // {
    //     color: 'red',
    //     aspectRatio: 120/145,
    //     aspect_ratio: '1:1',
    // },
    // {
    //     color: 'blue',
    //     aspectRatio: 90/145,
    //     aspect_ratio: '16:9',
    // },
    // {
    //     color: 'green',
    //     aspectRatio: 60/145,
    //     aspect_ratio: '4:5',
    // },
]

export const postData = [
    {
        id: 1,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 2,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 3,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 4,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 5,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 6,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 7,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 8,
        post_height: 100,
        post_width: 100,
        post_type: 'image',
        tagged_people: ['Zaid', 'Ahmed', ],
        vibe_tags: ['Photography', ],
        post_location: 'Bangalore, Karnataka',
        post_description: 'Post Description',
        post_url: 'https://images.unsplash.com/photo-1690207714547-6e76b0e61b40?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4fHx8ZW58MHx8fHx8&auto=format&fit=crop&w=800&q=60',
    },
]


export const onboarding_data = [
    {
        id: 1,
        image: media.shopping,
        title: 'Explore the New World',
        description: 'Find your best clothing option without any delay'
    },
    {
        id: 2,
        image: media.shopping_app,
        title: 'Discover New Trends',
        description: 'We provide a broad e-commerce app for our special cutomers'
    },
    {
        id: 3,
        image: media.deliveries,
        title: 'Get Faster Delivery',
        description: 'Fast delivery within 20 minutes to your home or office'
    },
]
