import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { addNewPost } from '../../global/api'
import { media } from '../../global/media'
import { screenWidth } from '../../global/constants'
import { locationTags, peoplesTag, vibeTags } from '../../global/sampleData'

import Input from '../../components/input'
import SavePostHeader from '../../components/header/SavePostHeader'

import RightArrowIcon from 'react-native-vector-icons/Feather'
import DownArrowIcon from 'react-native-vector-icons/Feather'

import TagIcon from 'react-native-vector-icons/AntDesign'
import MinusIcon from 'react-native-vector-icons/AntDesign'
import CloseIcon from 'react-native-vector-icons/AntDesign'
import LocationIcon from 'react-native-vector-icons/FontAwesome6'



const SavePostScreen = (props) => {

    const [buttonLoader, setButtonLoader] = useState(false);

    const [selectedFile, setSelectedFile] = useState('');
    const [aspectRatio, setAspectRatio] = useState('');
    const [postType, setPostType] = useState('');

    const [description, setDescription] = useState('');

    const [showPeopleTag, setShowPeopleTag] = useState(false);
    const [showLocationTag, setShowLocationTag] = useState(false);
    
    const [addedVibeTags, setAddedVibeTags] = useState([]);
    const [addedPeopleTag, setAddedPeopleTags] = useState([]);
    const [addedLocationTag, setAddedLocationTags] = useState('');

    useEffect(() => {
    
        setPostType(props.route.params?.image_details?.post_type)
        setSelectedFile(props.route.params?.image_details?.post_file)
        setAspectRatio(props.route.params?.image_details?.aspect_ratio)
      
    }, [])

    const filterPeopleTags = (tag) => {
        if(!addedPeopleTag.includes(tag)){       
            setAddedPeopleTags(oldArray => [...oldArray, tag]);  
        }else{
            var myArray = addedPeopleTag.filter(( obj ) => {
                return obj !== tag;
            });
            setAddedPeopleTags(myArray)
        }
    }

    const filterLocationTags = (tag) => {
        setAddedLocationTags(tag);  
        if(addedLocationTag == tag){       
            setAddedLocationTags('');  
        }else{
            setAddedLocationTags(tag)
        }
    }
    
    const filterVibeTags = (tag) => {
        if(!addedVibeTags.includes(tag)){       
            setAddedVibeTags(oldArray => [...oldArray, tag]);  
        }else{
            var myArray = addedVibeTags.filter(( obj ) => {
                return obj !== tag;
            });
            setAddedVibeTags(myArray)
        }
    }


    const addPostButtonFunction = () => {
        // console.log('====================================');
        // console.log("description => ", description);
        // console.log("addedPeopleTag => ", addedPeopleTag);
        // console.log("addedLocationTag => ", addedLocationTag);
        // console.log("addedVibeTags => ", addedVibeTags);
        // console.log("postType => ", postType);
        // console.log("aspectRatio => ", aspectRatio);
        // console.log("selectedFile => ", selectedFile);
        // console.log('====================================');

        setButtonLoader(true)

        var body = {
            post_type: postType,
            aspect_ratio: aspectRatio,
            tagged_people: addedPeopleTag,
            vibe_tags: addedVibeTags,
            post_location: addedLocationTag,
            post_description: description,
            post_url: selectedFile,
            post_poster_image: ''
        }

        addNewPost(body)
        .then((resp) => {
            console.log("resp =>> ",resp);
            setButtonLoader(false)
            props.navigation.navigate('Home')
        })
        .catch((err) => {
            setButtonLoader(false)
            console.log("err =>> ",err);

        })


    }

    return (
        <View style={styles.container} >
            <SavePostHeader buttonLoader={buttonLoader} addPostButton={() => {addPostButtonFunction()}} isVibeTag={addedVibeTags?.length !=0 ? true : false} />

            <ScrollView nestedScrollEnabled >
                <View style={styles.mainContainer} >
                    <Image source={{uri: selectedFile}} style={styles.imageStyle} />

                    <Input
                        isMultiline={true}
                        label="Description"
                        placeholder="Enter description  . .  ."
                        onChangeText={(text) => {setDescription(text)}}
                    />


                    <View style={styles.divider} />


                    <TouchableOpacity 
                        style={styles.rowSpace} 
                        onPress={() => setShowPeopleTag(!showPeopleTag)}
                    >
                        <View style={styles.row} >
                            <TagIcon name="tag" size={28} color={colors.primary1} style={styles.iconStyle} />
                            <Text style={styles.heading} >Tag People</Text>
                        </View>

                        {showPeopleTag
                        ?
                            <DownArrowIcon name="chevron-down" size={30} color={colors.dark_gray} style={styles.iconStyle} />
                        :
                            <RightArrowIcon name="chevron-right" size={30} color={colors.dark_gray} style={styles.iconStyle} />
                        }
                    </TouchableOpacity>

                    {showPeopleTag && (
                        <View>
                            <FlatList
                                data={peoplesTag}
                                keyExtractor={item => item.title}
                                horizontal
                                contentContainerStyle={{ marginTop: 6  }}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({item, index}) => (
                                    <TouchableOpacity 
                                        key={index} 
                                        style={styles.locationContainer} 
                                        onPress={() => filterPeopleTags(item.title)}
                                    >
                                        {addedPeopleTag.includes(item.title) ? <MinusIcon name="minuscircle" size={20} color={colors.black} style={{marginRight: 8}} /> : null}
                                        <Text style={styles.locationTitle} >{item.title}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    )}

                    <View style={styles.divider} />



                    <TouchableOpacity 
                        style={styles.rowSpace} 
                        onPress={() => setShowLocationTag(!showLocationTag)}
                    >
                        <View style={styles.row} >
                            <LocationIcon name="location-dot" size={28} color={colors.primary1} style={styles.iconStyle} />
                            <Text style={styles.heading} >Location</Text>
                        </View>

                        {showLocationTag
                        ?
                            <DownArrowIcon name="chevron-down" size={30} color={colors.dark_gray} style={styles.iconStyle} />
                        :
                            <RightArrowIcon name="chevron-right" size={30} color={colors.dark_gray} style={styles.iconStyle} />
                        }
                    </TouchableOpacity>

                    {showLocationTag && (<View>
                        <FlatList
                            data={locationTags}
                            keyExtractor={item => item.title}
                            horizontal
                            contentContainerStyle={{ marginTop: 6  }}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({item, index}) => (
                                <TouchableOpacity 
                                    key={index} 
                                    style={styles.locationContainer} 
                                    onPress={() => filterLocationTags(item.title)}
                                >
                                    {addedLocationTag == item.title ? <CloseIcon name="closecircle" size={20} color={colors.black} style={{marginRight: 8}} /> : null}
                                    <Text style={styles.locationTitle} >{item.title}</Text>
                                </TouchableOpacity>
                            )}
                        />
                    </View>)}

                    <View style={styles.divider} />


                    <View>
                        <Text style={[styles.heading, {marginLeft: 0, color: colors.dark_gray}]} >Add your vibetags</Text>
                    
                        <View>
                            <FlatList
                                data={vibeTags}
                                keyExtractor={item => item.title}
                                contentContainerStyle={{ marginBottom: 40, marginTop: 4, flexDirection: 'row', flexWrap: 'wrap',  }}
                                showsHorizontalScrollIndicator={false}
                                renderItem={({item, index}) => (
                                    <TouchableOpacity 
                                        key={index} 
                                        style={[styles.vibeContainer, {backgroundColor: addedVibeTags.includes(item.title) ? colors.primary1 : colors.white }]} 
                                        onPress={() => filterVibeTags(item.title)}
                                    >
                                        <Image source={item.icon} style={{height: 20, width: 20, marginRight: 4}} />
                                        <Text style={[styles.vibeheading, {color: addedVibeTags.includes(item.title) ? colors.white : colors.black }]} >{item.title}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>
                    </View>


                    
                
                </View>
            </ScrollView>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        width: '100%'
    },
    imageStyle: {
        height: 80, 
        width: 80,
        borderRadius: 10,
    },
    divider: {
        height: 1,
        width: '100%',
        marginVertical: 15,
        backgroundColor: colors.dark_gray,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rowSpace: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.primary1,
        marginLeft: 10
    },
    subheading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        marginTop: 4,
    },
    vibeheading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
    },
    vibeContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 40,
        marginTop: 10,
        marginRight: 10,
        paddingHorizontal: 12,
        paddingVertical: 4,
        borderColor: colors.primary1,
    },
    locationContainer: {
        backgroundColor: colors.white, 
        marginRight: 8,
        paddingVertical: 10,
        paddingHorizontal: 14,
        borderRadius: 50,
        borderWidth: 0.2,
        flexDirection: 'row',
        alignItems: 'center'
    }
})


export default SavePostScreen