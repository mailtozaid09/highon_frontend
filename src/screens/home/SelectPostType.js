import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList, Platform, PermissionsAndroid,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'

const SelectPostType = (props) => {

    const [postType, setPostType] = useState('');
    const [filePath, setFilePath] = useState({});

    useEffect(() => {
        console.log('====================================');
        setPostType(props.route.params?.postType)
        console.log('====================================');
    }, [])
    

    

    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                title: 'Camera Permission',
                message: 'App needs camera permission',
                },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
            console.warn(err);
            return false;
            }
        } else return true;
    };
  
    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                title: 'External Storage Write Permission',
                message: 'App needs write permission',
                },
            );
            // If WRITE_EXTERNAL_STORAGE Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
            }
            return false;
        } else return true;
    };
  
    const captureImage = async (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
            videoQuality: 'low',
            durationLimit: 30, //Video max duration in seconds
            saveToPhotos: true,
        };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            launchCamera(options, (response) => {
            console.log('Response = ', response);
  
            if (response.didCancel) {
                alert('User cancelled camera picker');
                return;
            } else if (response.errorCode == 'camera_unavailable') {
                alert('Camera not available on device');
                return;
            } else if (response.errorCode == 'permission') {
                alert('Permission not satisfied');
                return;
            } else if (response.errorCode == 'others') {
                alert(response.errorMessage);
                return;
            }
                setFilePath(response?.assets[0]);

                props.navigation.navigate('AddPostScreen', {selectedFile: response?.assets[0]?.uri, postType: postType})
            });
        }
    };
  
    const chooseFile = (type) => {
        let options = {
            mediaType: type,
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
    
            if (response.didCancel) {
            alert('User cancelled camera picker');
            return;
            } else if (response.errorCode == 'camera_unavailable') {
            alert('Camera not available on device');
            return;
            } else if (response.errorCode == 'permission') {
            alert('Permission not satisfied');
            return;
            } else if (response.errorCode == 'others') {
            alert(response.errorMessage);
            return;
            }
            setFilePath(response?.assets[0]);

            props.navigation.navigate('AddPostScreen', {selectedFile: response?.assets[0]?.uri, postType: postType})
        });
    };
  

    return (
        <View style={styles.container} >
            <TouchableOpacity 
                onPress={() => {chooseFile(postType == 'image' ? 'photo' : 'video')}}
                style={styles.optionContainer} >
                <Image source={media.gallery} style={styles.iconStyle} />
                <Text style={styles.heading} >Pick from gallery</Text>
            </TouchableOpacity>

            <View style={{height: 0.8, width: '100%', marginVertical: 10, backgroundColor: colors.gray}} />

            <TouchableOpacity 
                onPress={() => {captureImage(postType == 'image' ? 'photo' : 'video')}}
                style={styles.optionContainer} >
                <Image source={media.camera} style={styles.iconStyle} />
                <Text style={styles.heading} >Capture with camera</Text>
            </TouchableOpacity>


            <Image
                source={{uri: filePath.uri}}
                style={{height: 200, width: 200}}
            />
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    optionContainer: {
        width: '100%', 
        flexDirection: 'row', 
        alignItems: 'center',
        marginVertical: 15,
        paddingHorizontal: 25,
    },
    heading: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginLeft: 30,
    },
    iconStyle: {
        height: 24,
        width: 24,
    },
})


export default SelectPostType