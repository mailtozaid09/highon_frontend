import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import MenuIcon from 'react-native-vector-icons/Entypo'
import HeartIcon from 'react-native-vector-icons/AntDesign'
import { updatePostDetails } from '../../global/api'

const ViewPostDetails = (props) => {

    const [allPostDetails, setAllPostDetails] = useState([]);

    const [favourites, setFavourites] = useState([]);

    var dummyLocation = 'Bangalore'
    var avatarImage = 'https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8YXZhdGFyfGVufDB8fDB8fHww&auto=format&fit=crop&w=800&q=60'
    var avatarImage2 = 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTB8fGF2YXRhcnxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60'
    var avatarImage3 = 'https://images.unsplash.com/photo-1543610892-0b1f7e6d8ac1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fGF2YXRhcnxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60'
    var dummyDescription = 'Praveen Vanaparla. I didn’t realise the inner meaning and the feel in the film ( I mean subtitles) when I watched it in my…. childhood.After 10 years of the movie, today I realised that It is a brilliant piece of work from Trivikram Srinivas garu, Mahesh Babu garu, Shafi. Not to forget the wonderful background score and music from Mani Sharma garu. It will always be remembered as an iconic film in Telugu Film Industry even though the film couldn’t gross well. Do watch it if you are a lover of good films and do thank me later.'

    useEffect(() => {
        var arr = props.route.params?.postArray
        var id = props.route.params?.postId

        getCurrentPost(arr, id)
    }, [])

    const getCurrentPost = (arr, id) => {
        var newArr = arr
        var index = -1
        for(var i = 0; i < newArr.length; i++) {
            if(newArr[i].id === id) {
                index = i;
                break;
            }
        }
        
        const finalArray = newArr.slice(index)
        const imagePost = finalArray.filter(item => item.post_type != 'video' )
        setAllPostDetails(imagePost)
    }


    const updateFavourites = (item) => {

        if(!favourites.includes(item.id)){       
            setFavourites(oldArray => [...oldArray, item.id]);  
            console.log("adddd");
        }else{
            console.log("remove");
            var myArray = favourites.filter(( obj ) => {
                return obj !== item.id;
            });
            
            setFavourites(myArray)
        }

    //     var body = {
    //         is_favourite: !item?.is_favourite
    //     }

    //    updatePostDetails(body, item?.id)
    //    .then((resp) => {
    //         console.log("resp => ", resp);
    //    })
    //    .catch((err) => {
    //         console.log("err => ", err);
    //     })
    }


    return (
        <View style={styles.container} >
         
            <View style={styles.mainContainer} >
                <FlatList
                    data={allPostDetails}
                    key={item => item.id}
                    renderItem={({item, index}) => (
                        <View style={styles.postContainer} >
                            <View style={styles.rowSpace} >
                                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                    <View style={styles.avatarImageContainer} >
                                        <Image source={{uri: avatarImage}} style={styles.avatarImage} />
                                    </View>
                                    <View style={{marginLeft: 10, marginRight: 10, flex:0.9, }} >
                                        <Text style={styles.heading} >Username</Text>
                                        <Text numberOfLines={1} style={styles.subheading} >{item?.post_location || dummyLocation}. 20 min ago</Text>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {}}
                                >
                                    <MenuIcon name="dots-three-horizontal" size={24} color={colors.black} style={styles.iconStyle} />
                                </TouchableOpacity>
                            </View>

                            <View style={{width: '100%'}} >
                                <Image source={{uri: item?.post_url}} style={styles.postImage} />

                                <View style={{ position: 'absolute', right: 15, bottom: 15}} >
                                    {favourites.includes(item.id) ?
                                    <TouchableOpacity
                                        onPress={() => {updateFavourites(item)}}
                                    >
                                        <HeartIcon name="heart" size={24} color={colors.reddish} style={styles.iconStyle} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity
                                        onPress={() => {updateFavourites(item)}}
                                    >
                                        <HeartIcon name="hearto" size={24} color={colors.white} style={styles.iconStyle} />
                                    </TouchableOpacity>
                                    }
                                </View>
                            </View>

                            <View style={styles.rowSpace} >
                                <View style={styles.row} >
                                    <View style={styles.avatarImageContainer} >
                                        <Image source={{uri: avatarImage}} style={styles.avatarImage} />
                                    </View>
                                    <View style={[styles.avatarImageContainer, {position: 'absolute', right: -20}]} >
                                        <Image source={{uri: avatarImage2}} style={styles.avatarImage} />
                                    </View>
                                    <View style={[styles.avatarImageContainer, {position: 'absolute', right: -40}]} >
                                        <Image source={{uri: avatarImage3}} style={styles.avatarImage} />
                                    </View>
                                </View>
                                <View style={{flex: 1, marginLeft: 50,}} >
                                    <Text numberOfLines={1} style={styles.subheading} >Akhil Ready and 35 other emoted</Text>
                                </View>
                            </View>


                            <View style={[styles.rowSpace, {marginTop: -10, width: '100%'}]} >
                                <Text numberOfLines={2} style={[styles.subheading, {fontSize: 14,}]}>{item?.post_description || dummyDescription}</Text>
                            </View>
                        </View>
                    )}
                />
            </View>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1, 
        width: '100%',
        marginTop: 20,
    },
    postContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        margin: 15,
        marginTop: 0,
        marginBottom: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        lineHeight: 20,
    },
    subheading: {
        fontSize: 12,
        color: '#7B7B7B',
        fontFamily: Poppins.Medium,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    rowSpace: {
        width: '100%',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    postImage: {
        height: 200,
        width: '100%',
        borderRadius: 10,
    },
    avatarImageContainer: {
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 15,
        backgroundColor: colors.white,
    },
    avatarImage: {
        height: 24,
        width: 24,
        borderRadius: 12
    }
})


export default ViewPostDetails