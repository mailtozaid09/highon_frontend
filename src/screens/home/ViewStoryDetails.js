import React, { useRef, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Animated, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import ShareIcon from 'react-native-vector-icons/FontAwesome'
import HeartIcon from 'react-native-vector-icons/AntDesign'


import Video from 'react-native-video';
import  MediaControls, {PLAYER_STATES} from 'react-native-media-controls';
import { screenHeight, screenWidth } from '../../global/constants'


const ViewStoryDetails = (props) => {

    var counter = 0

    const countInterval = useRef(null);

    const [allPostDetails, setAllPostDetails] = useState([]);

    const [favourites, setFavourites] = useState([]);

    const [currentIdx, setCurrentIdx] = useState(0);

    const [progress, setProgress] = useState(new Animated.Value(0));

    const [progressCount, setProgressCount] = useState(0);

    var dummyLocation = 'Bangalore'
    var avatarImage = 'https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8YXZhdGFyfGVufDB8fDB8fHww&auto=format&fit=crop&w=800&q=60'
    var dummyDescription = 'Praveen Vanaparla. I didn’t realise the inner meaning and the feel in the film ( I mean subtitles) when I watched it in my…. childhood.After 10 years of the movie, today I realised that It is a brilliant piece of work from Trivikram Srinivas garu, Mahesh Babu garu, Shafi. Not to forget the wonderful background score and music from Mani Sharma garu. It will always be remembered as an iconic film in Telugu Film Industry even though the film couldn’t gross well. Do watch it if you are a lover of good films and do thank me later.'


    const videoPlayer = useRef(null);
 
 
    useEffect(() => {
        //console.log(props.route.params?.postArray);
        var arr = props.route.params?.postArray
        var id = props.route.params?.postId

        getCurrentPost(arr, id)

        countInterval.current = setInterval(() => setProgressCount((prev) => prev + 0.1), 1000);
        return () => {
            clearInterval(countInterval);
        };
    }, [])

    useEffect(() => {
        
        if (progressCount >= 1) {
          setProgressCount(0);
          clearInterval(countInterval);
        }
    }, [progressCount]);

    const getCurrentPost = (arr, id) => {
        var newArr = arr
        var index = -1
        for(var i = 0; i < newArr.length; i++) {
            if(newArr[i].id === id) {
                index = i;
                break;
            }
        }
        
        const finalArray = newArr.slice(index)
        const imagePost = finalArray.filter(item => item.post_type != 'image' )
        setAllPostDetails(imagePost)
        startVideoTimer(imagePost.length)
    }

    const startVideoTimer = (length) => {
        Animated.timing(progress, {
            toValue: screenWidth,
            duration: 5000,
        }).start();
       
        if(counter < length){
            setTimeout(() => {
                console.log("next video");
                counter = counter + 1
                progress.setValue(0)
                setCurrentIdx(counter)
                startVideoTimer(length)
                
            }, 5000);
        }else{
            props.navigation.goBack()
        }
        
    }

    const updateFavourites = (item) => {

        if(!favourites.includes(item.id)){       
            setFavourites(oldArray => [...oldArray, item.id]);  
            console.log("adddd");
        }else{
            console.log("remove");
            var myArray = favourites.filter(( obj ) => {
                return obj !== item.id;
            });
            
            setFavourites(myArray)
        }

    //     var body = {
    //         is_favourite: !item?.is_favourite
    //     }

    //    updatePostDetails(body, item?.id)
    //    .then((resp) => {
    //         console.log("resp => ", resp);
    //    })
    //    .catch((err) => {
    //         console.log("err => ", err);
    //     })
    }


    return (
        <View style={styles.container} >
         
            <View style={styles.mainContainer} >

                    <View style={{flex: 1, height: screenHeight, width: screenWidth, }}>
                            
                        <View style={styles.progressContainer}>
                            <Animated.View style={[styles.progressBar, { width: progress }]} />
                        </View>

                        <Video
                            ref={videoPlayer}
                            source={{ uri: allPostDetails[currentIdx]?.post_url}}
                            resizeMode='cover'
                            repeat={false}
                            playWhenInactive={true}
                            style={{
                                width: screenWidth,
                                height: screenHeight
                            }}
                            volume={50}
                        />

                        <View style={{position: 'absolute', height: '100%', width: '100%', justifyContent: 'flex-end', bottom: 0}} >
                            <View style={{paddingHorizontal: 15, }}>
                                <View style={{alignItems: 'flex-end'}} >
                                    <View style={{}} >
                                        {favourites.includes(allPostDetails[currentIdx]?.id) ?
                                        <TouchableOpacity
                                            onPress={() => {updateFavourites(allPostDetails[currentIdx])}}
                                        >
                                            <HeartIcon name="heart" size={30} color={colors.reddish} style={styles.iconStyle} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity
                                            onPress={() => {updateFavourites(allPostDetails[currentIdx])}}
                                        >
                                            <HeartIcon name="hearto" size={30} color={colors.white} style={styles.iconStyle} />
                                        </TouchableOpacity>
                                        }
                                    </View>
                                    <TouchableOpacity
                                        onPress={() => {}}
                                    >
                                        <ShareIcon name="share" size={30} color={colors.white} style={[styles.iconStyle, {marginTop: 30}]} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                    <View style={styles.avatarImageContainer} >
                                        <Image source={{uri: avatarImage}} style={styles.avatarImage} />
                                    </View>
                                    <View style={{marginLeft: 10, marginRight: 10, flex:0.9, }} >
                                        <Text style={styles.heading} >Username</Text>
                                        <Text numberOfLines={1} style={styles.subheading} >{allPostDetails[currentIdx]?.post_location || dummyLocation}. 20 min ago</Text>
                                    </View>
                                </View>
                                <View style={[styles.rowSpace, {marginTop: -10, marginBottom: 20, width: '100%'}]} >
                                    <Text numberOfLines={3} style={[styles.subheading, {fontSize: 14,}]}>{allPostDetails[currentIdx]?.post_description || dummyDescription}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
            </View>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1, 
        width: '100%',
    },
    postContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        margin: 15,
        marginTop: 0,
        marginBottom: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
        lineHeight: 20,
    },
    subheading: {
        fontSize: 12,
        color: colors.white,
        fontFamily: Poppins.Medium,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    rowSpace: {
        width: '100%',
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    postImage: {
        height: 200,
        width: '100%',
        borderRadius: 10,
    },
    avatarImageContainer: {
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 15,
        backgroundColor: colors.white,
    },
    avatarImage: {
        height: 24,
        width: 24,
        borderRadius: 12
    },
    progressContainer: {
        height: 5,
        backgroundColor: '#ccc',
    },
    progressBar: {
        height: 5,
        backgroundColor: colors.primary,
    },
})


export default ViewStoryDetails
