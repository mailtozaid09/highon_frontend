import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { getAllPost } from '../../global/api'

import PostItem from '../../components/custom/PostItem'
import HomeHeader from '../../components/header/HomeHeader'
import AddPostModal from '../../components/modal/AddPostModal'



const HomeScreen = ({navigation}) => {

    const [addPostModal, setAddPostModal] = useState(false);

    const [postArray, setPostArray] = useState([]);


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getAllPostFunction()
        });
        
        return unsubscribe;
      }, [navigation]);


    const getAllPostFunction = () => {

        getAllPost()
        .then((resp) => {
            //console.log("resp =>> ",resp);
            setPostArray(resp)
        })
        .catch((err) => {
            console.log("err =>> ",err);
        })
    }

    return (
        <View style={styles.container} >
            <HomeHeader addPostButton={() => {setAddPostModal(true)}} />

            <View style={{width: '100%',}} >
                <ScrollView>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 50, padding: 20 }} >
                        <View>
                            {postArray?.filter((_, i) => i % 2 === 0)
                            .map((item, index) => (
                                <PostItem key={index}  postArray={postArray} index={index} object={item} />
                            ))}
                        </View>
                        <View>
                            {postArray?.filter((_, i) => i % 2 !== 0)
                            .map((item, index) => (
                                <PostItem key={index} postArray={postArray} index={index} object={item} />
                            ))}
                        </View>
                    </View>
                </ScrollView>
            </View>
            
            {addPostModal && <AddPostModal closeModal={() => {setAddPostModal(false)}} />}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white,
    },
})

export default HomeScreen

