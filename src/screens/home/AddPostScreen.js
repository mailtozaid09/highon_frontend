import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, FlatList,  } from 'react-native'

import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'
import AddPostHeader from '../../components/header/AddPostHeader'
import { screenWidth } from '../../global/constants'

const AddPostScreen = (props) => {

    const [selectedFile, setSelectedFile] = useState('');
    const [postType, setPostType] = useState('');
    
    const [activeRatio, setActiveRatio] = useState('1:1');

    useEffect(() => {
        
        setSelectedFile(props.route.params?.selectedFile)
        setPostType(props.route.params?.postType)
        
    }, [])


    const nextButton = () => {

        var imageDetails = {
            post_file: selectedFile,
            post_type: postType,
            aspect_ratio: activeRatio,
        }
        props.navigation.navigate('SavePostScreen', {image_details: imageDetails})
    }


    return (
        <View style={styles.container} >
            <AddPostHeader nextButton={() => {nextButton()}} />

            <View style={styles.mainContainer} >
                <View style={styles.imageContainer} >
                    <Image 
                        source={{uri: selectedFile}} 
                        style={
                            activeRatio == '1:1' ? styles.onebyoneSelected 
                            : activeRatio == '4:5' ? styles.fourbyfiveSelected  
                            : activeRatio == '16:9' ? styles.sixteenbynineSelected
                            : styles.onebyone 
                        } 
                    />
                </View>

                <View style={styles.ratioContainer} >
                    <Text style={styles.heading} >Aspect Ratio</Text>
                
                    <View style={{width: '100%', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-evenly'}} >
                        <TouchableOpacity 
                            onPress={() => {setActiveRatio('1:1')}}
                            style={styles.centerItem} >
                            <View style={[styles.onebyone, {borderColor: activeRatio == '1:1' ? colors.primary : colors.dark_gray}]} />
                            <Text style={[styles.subheading, {color: activeRatio == '1:1' ? colors.primary : colors.dark_gray}]} >1:1</Text>
                        </TouchableOpacity>

                        <TouchableOpacity 
                            onPress={() => {setActiveRatio('4:5')}}
                            style={styles.centerItem} >
                            <View style={[styles.fourbyfive, {borderColor: activeRatio == '4:5' ? colors.primary : colors.dark_gray}]} />
                            <Text style={[styles.subheading, {color: activeRatio == '4:5' ? colors.primary : colors.dark_gray}]} >4:5</Text>
                        </TouchableOpacity>

                        <TouchableOpacity 
                            onPress={() => {setActiveRatio('16:9')}}
                            style={styles.centerItem} >
                            <View style={[styles.sixteenbynine, {borderColor: activeRatio == '16:9' ? colors.primary : colors.dark_gray}]} />
                            <Text style={[styles.subheading, {color: activeRatio == '16:9' ? colors.primary : colors.dark_gray}]} >16:9</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1, 
        width: '100%',
        justifyContent: 'space-between', 
    },
    imageContainer: {
        width: '100%', 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EFEFEF',
        paddingHorizontal: 20,
        paddingVertical: 80,
    },
    ratioContainer: {
        width: '100%', 
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EFEFEF',
        padding: 15,
        paddingBottom: 25,
    },
    heading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: '#7B7B7B',
        marginBottom: 15,
    },
    subheading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        marginTop: 4,
    },
    centerItem: {
        alignItems: 'center'
    },
    onebyone: {
        height: 35,
        width: 35,
        borderWidth: 1,
        borderRadius: 4,
    },
    fourbyfive: {
        height: 50,
        width: 35,
        borderWidth: 1,
        borderRadius: 4,
    },
    sixteenbynine: {
        height: 35,
        width: 70,
        borderWidth: 1,
        borderRadius: 4,
    },
    onebyoneSelected: {
        height: screenWidth*0.65,
        width: screenWidth*0.65,
        borderRadius: 15,
    },
    fourbyfiveSelected: {
        height: screenWidth*0.7,
        width: screenWidth*0.5,
        borderRadius: 15,
    },
    sixteenbynineSelected: {
        height: screenWidth*0.5,
        width: screenWidth*0.7,
        borderRadius: 15,
    },
})


export default AddPostScreen