import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView,  } from 'react-native'

import { colors } from '../../global/colors'


const OnboardingScreen = ({navigation}) => {

    useEffect(() => {
        
    }, [])

    return (
        <View style={styles.container} >
            <Text>OnboardingScreen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    }
})

export default OnboardingScreen

