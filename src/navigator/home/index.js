import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../screens/home';
import SelectPostType from '../../screens/home/SelectPostType';
import AddPostScreen from '../../screens/home/AddPostScreen';
import SavePostScreen from '../../screens/home/SavePostScreen';
import ViewPostDetails from '../../screens/home/ViewPostDetails';
import ViewStoryDetails from '../../screens/home/ViewStoryDetails';


const Stack = createStackNavigator();


const HomeStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    // headerLeft: () => null,
                    // headerTitle: 'Home',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="SelectPostType"
                component={SelectPostType}
                options={{
                    headerShown: true,
                    headerTitle: '',
                }}
            />
            <Stack.Screen
                name="AddPostScreen"
                component={AddPostScreen}
                options={{
                    headerShown: false,
                    headerTitle: '',
                }}
            />
            <Stack.Screen
                name="SavePostScreen"
                component={SavePostScreen}
                options={{
                    headerShown: false,
                    headerTitle: '',
                }}
            />
            <Stack.Screen
                name="ViewPostDetails"
                component={ViewPostDetails}
                options={{
                    headerShown: true,
                    headerTitle: '',
                }}
            />
            <Stack.Screen
                name="ViewStoryDetails"
                component={ViewStoryDetails}
                options={{
                    headerShown: true,
                    headerTitle: '',
                }}
            />



        </Stack.Navigator>
    );
}

export default HomeStack