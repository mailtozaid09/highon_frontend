import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import OnboardingStack from './onboarding';
import LoginStack from './login';
import Tabbar from './tabbar';
import HomeStack from './home';


const Stack = createStackNavigator();


const Navigator = ({navgation}) => {

    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        //getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        // let user = await AsyncStorage.getItem('user_details');  
        // let user_details = JSON.parse(user);  
        // setUserDetails(user_details)

        // console.log('====================================');
        // console.log("navigator ", user_details);
        // console.log('====================================');
    }
   


    return (
        <Stack.Navigator 
        initialRouteName='HomeStack'    
        >
            {/* <Stack.Screen
                name="OnboardingStack"
                component={OnboardingStack}
                options={{
                    headerShown: false
                }}
            /> 
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            /> */}
            <Stack.Screen
                name="HomeStack"
                component={HomeStack}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}

export default Navigator