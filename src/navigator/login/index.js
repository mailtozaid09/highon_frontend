import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../../screens/login';


const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Login" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />

        </Stack.Navigator>
    );
}

export default LoginStack