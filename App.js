import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

import Navigator from './src/navigator';

import { Provider } from 'react-redux';
import Store from './src/redux/store';

LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])
    
    return (
        <Provider store={Store}>
            <NavigationContainer>
                <StatusBar backgroundColor = "#0D0D0D"   />  
                <Navigator />
            </NavigationContainer>
        </Provider>
    )
}

export default App
